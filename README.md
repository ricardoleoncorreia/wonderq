# WonderQ

This project is part of an engineering take home exercise. The problem states as follows:

> WonderQ is a broker that allows producers to write to it, and consumers to
> read from it. It runs on a single server and has a single message queue.
> Whenever a producer writes to WonderQ, a message ID is generated and
> returned as confirmation. Whenever a consumer polls WonderQ for new
> messages, it can get those messages. These messages should NOT be available
> for processing by any other consumer that may be concurrently accessing
> WonderQ.

> NOTE that, when a consumer gets a set of messages, it must notify WonderQ
> that it has processed each message (individually). This deletes that
> message from the WonderQ database. If a message is received by a consumer
> but NOT marked as processed within a configurable amount of time, the
> message then becomes available to any consumer requesting again.

> Tasks:

> - Build a module that represents WonderQ. It should have a backend REST
>   API, with endpoints that producers/consumers could use to generate and
>   consume messages.

> Please store data in-memory using Javascript data structures rather than
> using a database or Redis. Use Node.js ES6+ with Typescript.
> This module is meant to run locally - you do not need to support endpoint
> authentication or a deployment process.

> - Add tests for the critical functionality of WonderQ.

> - Write brief documentation for your API endpoints.

# How to run locally

1. Download the [WonderQ](https://gitlab.com/ricardoleoncorreia/wonderq) GitLab repo.
2. Move into the folder and run `npm i`.
3. Run `npm run start:dev`.

After these steps, the server will be ready to consume using `http://localhost:3000`.

# Documentation

This project contains a Swagger UI. You can access going into http://localhost:3000/docs.

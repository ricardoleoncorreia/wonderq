import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsString, Min } from 'class-validator';

export class EnqueueDTO {
  @IsString()
  @ApiProperty({
    example: 'Sample message',
    description: 'Message to enqueue in the broker',
  })
  message!: string;
}

export class DequeueDTO {
  @IsString()
  @ApiProperty({
    example: 'c52a3a48-738e-4a37-a39a-902c9bd05bf5',
    description: 'ID generated for the message added to the queue',
  })
  id!: string;
}

export class VisibilityTimeoutDTO {
  @Min(2000)
  @IsNumber()
  @ApiProperty({
    example: 10000,
    description: 'Visibility timeout value in milliseconds',
  })
  timeout!: number;
}

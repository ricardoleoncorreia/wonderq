import { ApiProperty } from '@nestjs/swagger';

export class QueueMessageIDEntity {
  @ApiProperty({
    example: 'ebccff7b-74a3-44d5-bcb7-c0cb88eb7a9c',
    description: 'ID generated for the message added to the queue',
  })
  id: string;
}

export class QueueMessageEntity {
  @ApiProperty({
    example: 'ebccff7b-74a3-44d5-bcb7-c0cb88eb7a9c',
    description: 'ID generated for the message added to the queue',
  })
  id: string;

  @ApiProperty({
    example: 'sample message',
    description: 'Message to enqueue in the broker',
  })
  message: string;

  @ApiProperty({ example: 1639376220640, description: 'Last polled timestamp' })
  lastPolled?: number;
}

export class DequeuedConfirmationEntity {
  @ApiProperty({ example: true, description: 'Result of the dequeued process' })
  dequeued: boolean;
}

export class VisibilityTimeoutUpdateEntity {
  @ApiProperty({
    example: true,
    description: 'Result of the visibility timeout update request',
  })
  updated: boolean;
}

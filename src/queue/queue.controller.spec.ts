import { Test, TestingModule } from '@nestjs/testing';
import {
  QueueMessageEntity,
  QueueMessageIDEntity,
} from './namespace/queue.entities';
import { QueueController } from './queue.controller';
import { QueueService } from './queue.service';

const Mocks = {
  firstMessage: { message: 'myFirstMessage' },
  secondMessage: { message: 'myFirstMessage' },
};

describe('QueueController', () => {
  let queueController: QueueController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [QueueController],
      providers: [QueueService],
    }).compile();

    queueController = app.get<QueueController>(QueueController);
  });

  describe('when producer sends a message', () => {
    let enqueueResponse: QueueMessageIDEntity;

    beforeEach(() => {
      enqueueResponse = queueController.enqueue(Mocks.firstMessage);
    });

    it('should queue the message', () => {
      const messages = queueController.peek();

      expect(messages.length).toBe(1);
      expect(messages[0].message).toBe(Mocks.firstMessage.message);
    });

    it('should return an ID for the message', () => {
      expect(enqueueResponse.id).toBeDefined();
    });
  });

  describe('when the queue has two messages', () => {
    let firstMessageId: QueueMessageIDEntity;
    let secondMessageId: QueueMessageIDEntity;

    beforeEach(() => {
      firstMessageId = queueController.enqueue(Mocks.firstMessage);
      secondMessageId = queueController.enqueue(Mocks.secondMessage);
    });

    describe('and first consumer polls messages', () => {
      let pollResponse: QueueMessageEntity[];

      beforeEach(() => {
        pollResponse = queueController.peek();
      });

      it('should receive two messages', () => {
        expect(pollResponse.length).toBe(2);
        expect(pollResponse[0].id).toBe(firstMessageId.id);
        expect(pollResponse[1].id).toBe(secondMessageId.id);
      });

      describe(`and visibility timeout is 10 seconds`, () => {
        const visibilityTimeoutInMilliseconds = 10000;

        describe('and visibility times out', () => {
          beforeEach(() => {
            const pollDates = pollResponse.map((message) => message.lastPolled);
            const maxPollDate: number = Math.max(...pollDates);
            const timeoutDate: number =
              maxPollDate + visibilityTimeoutInMilliseconds + 1;
            jest.spyOn(Date, 'now').mockImplementation(() => timeoutDate);
          });

          describe('and first consumer ends the process for each message', () => {
            it('should NOT allow to dequeue the messages', () => {
              const firstMessageCall = () =>
                queueController.dequeue(firstMessageId);
              expect(firstMessageCall).toThrowError();

              const secondMessageCall = () =>
                queueController.dequeue(secondMessageId);
              expect(secondMessageCall).toThrowError();
            });
          });

          describe('and a second consumer polls for messages', () => {
            it('should get two messages', () => {
              const newPoll = queueController.peek();
              expect(newPoll.length).toBe(2);
              expect(newPoll[0].id).toBe(firstMessageId.id);
              expect(newPoll[1].id).toBe(secondMessageId.id);
            });
          });
        });

        describe('and visibility does NOT time out', () => {
          describe('and first consumer ends the process for each message', () => {
            it('should allow to dequeue the messages', () => {
              const firstMessageDequeueConfirmation =
                queueController.dequeue(firstMessageId);
              expect(firstMessageDequeueConfirmation.dequeued).toBeTruthy();

              const secondMessageDequeueConfirmation =
                queueController.dequeue(secondMessageId);
              expect(secondMessageDequeueConfirmation.dequeued).toBeTruthy();
            });
          });

          describe('and a second consumer polls for messages', () => {
            it('should get NO messages', () => {
              const newPoll = queueController.peek();
              expect(newPoll).toEqual([]);
            });
          });
        });
      });
    });
  });
});

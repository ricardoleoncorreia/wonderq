import {
  BadRequestException,
  ConflictException,
  Injectable,
} from '@nestjs/common';
import { v4 as uuidv4 } from 'uuid';
import {
  DequeuedConfirmationEntity,
  QueueMessageEntity,
  QueueMessageIDEntity,
  VisibilityTimeoutUpdateEntity,
} from './namespace/queue.entities';

@Injectable()
export class QueueService {
  private queue: QueueMessageEntity[] = [];
  private visibilityTimeout = 10000; // in milliseconds (TODO: make configurable)

  peek(): QueueMessageEntity[] {
    const messagesToPoll: QueueMessageEntity[] = [];

    this.queue = this.queue.map((element) => {
      if (!element.lastPolled || this.elementTimedOut(element.lastPolled)) {
        element.lastPolled = Date.now();
        messagesToPoll.push(element);
      }
      return element;
    });

    return messagesToPoll;
  }

  enqueue(message: string): QueueMessageIDEntity {
    const id = uuidv4();
    this.queue.push({ id, message });
    return { id } as QueueMessageIDEntity;
  }

  dequeue(id: string): DequeuedConfirmationEntity {
    const index = this.queue.findIndex((element) => element.id === id);

    if (index === -1) {
      throw new BadRequestException(`Task with ID ${id} does not exist`);
    }
    if (!this.queue[index].lastPolled) {
      const errorMessage = `Task with ID ${id} was not previously polled`;
      throw new BadRequestException(errorMessage);
    }
    if (this.elementTimedOut(this.queue[index].lastPolled)) {
      throw new ConflictException('Time to dequeue the task expired');
    }

    this.queue.splice(index, 1);
    return { dequeued: true };
  }

  private elementTimedOut(lastPolled: number): boolean {
    return lastPolled + this.visibilityTimeout < Date.now();
  }

  updateVisibilityTimeout(
    visibilityTimeout: number,
  ): VisibilityTimeoutUpdateEntity {
    this.visibilityTimeout = visibilityTimeout;
    return { updated: true };
  }
}

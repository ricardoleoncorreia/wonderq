import { Body, Controller, Delete, Get, Post, Put } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import {
  DequeueDTO,
  EnqueueDTO,
  VisibilityTimeoutDTO,
} from './namespace/queue.dto';
import {
  DequeuedConfirmationEntity,
  QueueMessageEntity,
  QueueMessageIDEntity,
  VisibilityTimeoutUpdateEntity,
} from './namespace/queue.entities';
import { QueueService } from './queue.service';

@ApiTags('queue')
@Controller('/queue')
export class QueueController {
  constructor(private readonly queueService: QueueService) {}

  @Get('/')
  @ApiOperation({ summary: 'Polls messages' })
  @ApiResponse({
    status: 200,
    description: 'Returns a list of messages',
    type: QueueMessageEntity,
    isArray: true,
  })
  peek(): QueueMessageEntity[] {
    return this.queueService.peek();
  }

  @Post('/')
  @ApiOperation({ summary: 'Enqueue message' })
  @ApiResponse({
    status: 201,
    description: 'Returns the ID of the enqueued message',
    type: QueueMessageIDEntity,
  })
  enqueue(@Body() body: EnqueueDTO): QueueMessageIDEntity {
    return this.queueService.enqueue(body.message);
  }

  @Delete('/')
  @ApiOperation({ summary: 'Dequeue message' })
  @ApiResponse({
    status: 204,
    description: 'Returns the result of the dequeued request',
    type: DequeuedConfirmationEntity,
  })
  dequeue(@Body() body: DequeueDTO): DequeuedConfirmationEntity {
    return this.queueService.dequeue(body.id);
  }

  @Put('/timeout')
  @ApiOperation({ summary: 'Update visibility timeout value' })
  @ApiResponse({
    status: 200,
    description: 'Returns the result of the visibility timeout update request',
    type: VisibilityTimeoutUpdateEntity,
  })
  updateVisibilityTimeout(
    @Body() body: VisibilityTimeoutDTO,
  ): VisibilityTimeoutUpdateEntity {
    return this.queueService.updateVisibilityTimeout(body.timeout);
  }
}
